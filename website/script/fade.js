// http://jsfiddle.net/htmled/2kkHH/
$(document).ready(function() {
	$('.fadein div').filter(function(index) {
		return index != 0;
	}).css({opacity: 0});

	var numberOfElements = $('.fadein div').size();

	var currentIndex = 0;
	setInterval(function () {
	    var current = $('.fadein div').eq(currentIndex);

	    currentIndex = (currentIndex + 1) % numberOfElements;
	    var next = $('.fadein div').eq(currentIndex);

	    current.animate({opacity: 0}, 600);
	    next.animate({opacity: 1}, 600);
		
	}, 5000); // 4 seconds

	// Modal dialog
	fixModalSize();
	$('.download button').click(function() {
		fixModalSize();
		$('html').addClass('show-modal');
	});
	$('.close').click(function() {
		$('html').removeClass('show-modal');
	});
	$('.modal-shade').click(function() {
		$('html').removeClass('show-modal');
	});
});

function fixModalSize() {
	var pageWidth = $('html').width();
	var modalWidth = $('.modal').width();
	$('.modal').css('left', (pageWidth / 2) - (modalWidth / 2));
}