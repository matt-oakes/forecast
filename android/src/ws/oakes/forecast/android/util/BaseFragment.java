package ws.oakes.forecast.android.util;

import com.squareup.otto.Subscribe;

import com.google.gson.Gson;

import ws.oakes.forecast.android.R;
import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastFlags;
import ws.oakes.forecast.common.util.ForecastBus;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Provides a base for saving and restoring the state of the weather forecast UI.
 * All sub classes must include this code to make the event bus work correctly:
 *
 *     @Override
 *     public void onResume() {
 *         super.onResume();
 *         // Register on the event bus
 *         eventBus.register(this);
 *     }
 *
 *     @Override
 *     public void onPause() {
 *         super.onPause();
 *         // Unregister from the event bus
 *         eventBus.unregister(this);
 *     }
 *
 *     @Subscribe
 *     public void onNewForecast(ForecastAvailableEvent forecastEvent) {
 *         // Get the new forecast and update the UI
 *         displayedForecast = forecastEvent.getForecast();
 *         updateForecastDisplay();
 *     }
 *
 **/
public abstract class BaseFragment extends Fragment {
    private static final String DISPLAYED_FORECAST_EXTRA = "ws.oakes.forecast.DISPLAYED_FORECAST_EXTRA";

    // Stores a copy of the shared event bus
    protected ForecastBus eventBus;
    // A copy of the forecast we're displaying
    protected Forecast displayedForecast;

    public BaseFragment() {
        // Get the shared event bus
        eventBus = ForecastBus.get();
    }

    @Override
    public void onViewStateRestored(Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);

        // Check if we have a saved forecast object
        if (savedInstanceState != null ) {
            // Get any store json string
            String jsonForecast = savedInstanceState.getString(DISPLAYED_FORECAST_EXTRA);
            if (jsonForecast != null) {
                // Convert to a forecast object again
                displayedForecast = new Gson().fromJson(jsonForecast, Forecast.class);
                // Update the text views
                updateForecastDisplay();
            }
        }
    }

    @Override
    public void onSaveInstanceState (Bundle outState) {
        // Store the forecast as a json string
        String jsonForecast = new Gson().toJson(displayedForecast);
        outState.putString(DISPLAYED_FORECAST_EXTRA, jsonForecast);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the default layout
        View rootView = inflater.inflate(R.layout.fragment_not_implemented, container, false);
        return rootView;
    }

    /**
     * Updates the UI with the current displayed forecast
     **/
    protected abstract void updateForecastDisplay();
}
