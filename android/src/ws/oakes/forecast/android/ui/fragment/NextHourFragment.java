package ws.oakes.forecast.android.ui.fragment;

import com.squareup.otto.Subscribe;

import com.google.gson.Gson;

import ws.oakes.forecast.android.R;
import ws.oakes.forecast.android.util.BaseFragment;
import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastFlags;
import ws.oakes.forecast.common.util.ForecastAvailableEvent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class NextHourFragment extends BaseFragment {
    public NextHourFragment() {
        super();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register on the event bus
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister from the event bus
        eventBus.unregister(this);
    }

    @Subscribe
    public void onNewForecast(ForecastAvailableEvent forecastEvent) {
        // Get the new forecast and update the UI
        displayedForecast = forecastEvent.getForecast();
        updateForecastDisplay();
    }

    /**
     * Updates the UI with the current displayed forecast
     **/
    @Override
    protected void updateForecastDisplay() {
        
    }
}
