package ws.oakes.forecast.android.ui.fragment;

import com.squareup.otto.Subscribe;

import com.google.gson.Gson;

import ws.oakes.forecast.android.R;
import ws.oakes.forecast.android.util.BaseFragment;
import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastFlags;
import ws.oakes.forecast.common.util.ForecastAvailableEvent;
import ws.oakes.forecast.common.util.ForecastUtils;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class SummaryFragment extends BaseFragment {
    // The weather text views
    private TextView nowSummary;
    private TextView nowTemperature;
    private TextView nextHour;
    private TextView nextDay;
    private TextView nextWeek;

    public SummaryFragment() {
        super();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register on the event bus
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister from the event bus
        eventBus.unregister(this);
    }

    @Subscribe
    public void onNewForecast(ForecastAvailableEvent forecastEvent) {
        // Get the new forecast and update the UI
        displayedForecast = forecastEvent.getForecast();
        updateForecastDisplay();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout and get the text view
        View rootView = inflater.inflate(R.layout.fragment_today, container, false);
        nowSummary = (TextView) rootView.findViewById(R.id.now_summary);
        nowTemperature = (TextView) rootView.findViewById(R.id.now_temperature);
        nextHour = (TextView) rootView.findViewById(R.id.next_hour_summary);
        nextDay = (TextView) rootView.findViewById(R.id.next_24_hours_summary);
        nextWeek = (TextView) rootView.findViewById(R.id.next_7_days_summary);

        return rootView;
    }

    /**
     * Updates the UI with the current displayed forecast
     **/
    @Override
    protected void updateForecastDisplay() {
        // Get all the values we need
        // TODO: Need to check for null values
        String summary = displayedForecast.currently.summary;
        String nextHourSummary = displayedForecast.minutely.summary;
        String nextDaySummary = displayedForecast.hourly.summary;
        String nextWeekSummary = displayedForecast.daily.summary;
        String temperature = ForecastUtils.getTemperatureString(getActivity().getApplicationContext(),
                                                                displayedForecast.currently.temperature,
                                                                displayedForecast.flags.units);

        // Update all the text views
        nowSummary.setText(summary);
        nowTemperature.setText(temperature);
        nextHour.setText(nextHourSummary);
        nextDay.setText(nextDaySummary);
        nextWeek.setText(nextWeekSummary);
    }
}
