package ws.oakes.forecast.android.ui.fragment;

import com.squareup.otto.Subscribe;

import com.google.gson.Gson;

import ws.oakes.forecast.android.R;
import ws.oakes.forecast.android.util.BaseFragment;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastAvailableEvent;
import ws.oakes.forecast.common.util.ForecastUtils;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NextWeekFragment extends BaseFragment {
    // The weather text views
    private ListView listView;
    // An adapter to use for the list view
    private WeatherAdapter adapter;

    public NextWeekFragment() {
        super();
    }

    @Override
    public void onResume() {
        super.onResume();
        // Register on the event bus
        eventBus.register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        // Unregister from the event bus
        eventBus.unregister(this);
    }

    @Subscribe
    public void onNewForecast(ForecastAvailableEvent forecastEvent) {
        // Get the new forecast and update the UI
        displayedForecast = forecastEvent.getForecast();
        updateForecastDisplay();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout and get the list view
        View rootView = inflater.inflate(R.layout.fragment_next_week, container, false);
        listView = (ListView) rootView.findViewById(R.id.list_view);

        // Set the list view adapter
        adapter = new WeatherAdapter(getActivity());
        listView.setAdapter(adapter);

        return rootView;
    }

    /**
     * Updates the UI with the current displayed forecast
     **/
    @Override
    protected void updateForecastDisplay() {
        // Create a copy of the list for forecasts
        List<ForecastDataPoint> days = new ArrayList<ForecastDataPoint>(displayedForecast.daily.data);
        // Set the new data
        adapter.setData(days);
    }

    private class WeatherAdapter extends BaseAdapter {
        private List<ForecastDataPoint> days;
        private LayoutInflater inflater;

        public WeatherAdapter(Context context) {
            super();
            days = new ArrayList<ForecastDataPoint>();
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        public void setData(List<ForecastDataPoint> newData) {
            days = newData;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return days.size();
        }

        @Override
        public Object getItem(int position) {
            return days.get(position);
        }

        @Override
        public long getItemId(int position) {
            return days.get(position).time;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_weather_day, parent, false);

                holder = new Holder();
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.summary = (TextView) convertView.findViewById(R.id.summary);
                holder.temperature = (TextView) convertView.findViewById(R.id.temperature);

                convertView.setTag(holder);
            }
            else {
                holder = (Holder) convertView.getTag();
            }

            ForecastDataPoint data = days.get(position);

            // Set the icon
            if (ForecastUtils.iconMap.containsKey(data.icon)) {
                int iconDrawable = ForecastUtils.iconMap.get(data.icon);
                holder.icon.setImageResource(iconDrawable);
            }
            else {
                // TODO: Should have a default
            }

            // Set the date
            String dateString;
            if (position == 0) {
                dateString = getString(R.string.today);
            }
            else if (position == 1) {
                dateString = getString(R.string.tomorrow);
            }
            else {
                Calendar calendar = Calendar.getInstance();
                calendar.setTimeInMillis(data.time * 1000);
                Date date = calendar.getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEEEEEEEE");
                dateString = dateFormat.format(date);
            }
            holder.date.setText(dateString);

            // Set the summary
            holder.summary.setText(data.summary);

            // Set the temperature
            String temperature = ForecastUtils.getTemperatureString(getActivity().getApplicationContext(),
                                                                    data.temperatureMax,
                                                                    displayedForecast.flags.units);
            holder.temperature.setText(temperature);

            return convertView;
        }

        private class Holder {
            ImageView icon;
            TextView date;
            TextView summary;
            TextView temperature;
        }
    }
}
