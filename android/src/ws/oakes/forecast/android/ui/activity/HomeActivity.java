package ws.oakes.forecast.android.ui.activity;

import com.astuetz.viewpager.extensions.PagerSlidingTabStrip;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import ws.oakes.forecast.android.R;
import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastApi;
import ws.oakes.forecast.common.api.ForecastApiBuilder;
import ws.oakes.forecast.common.api.geolocation.Geolocation;
import ws.oakes.forecast.common.api.geolocation.GeolocationApi;
import ws.oakes.forecast.common.api.geolocation.GeolocationApiBuilder;
import ws.oakes.forecast.common.api.geolocation.Geolocations;
import ws.oakes.forecast.common.api.geolocation.SearchLocation;
import ws.oakes.forecast.common.api.geolocation.SearchLocations;
import ws.oakes.forecast.android.ui.fragment.NextDayFragment;
import ws.oakes.forecast.android.ui.fragment.NextHourFragment;
import ws.oakes.forecast.android.ui.fragment.NextWeekFragment;
import ws.oakes.forecast.android.ui.fragment.SummaryFragment;
import ws.oakes.forecast.common.util.ForecastAvailableEvent;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastService;
import ws.oakes.forecast.common.util.ForecastService.ForecastBinder;
import ws.oakes.forecast.common.util.LocationAvailableEvent;
import ws.oakes.forecast.common.util.PlayServicesErrorEvent;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import android.location.Location;
import android.app.Activity;
import android.app.ActionBar;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Resources;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuInflater;
import android.widget.SearchView;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class HomeActivity extends FragmentActivity implements ServiceConnection {
    // The API key for Forecast.io
    private static final String API_KEY = "2298603da4018bc4045e8d0df1cbb931";
    // The username for Geonames
    private static final String GEOCODER_USERNAME = "matto1990";
    // Constants for the saved state
    private static final String DISPLAY_CURRENT_LOCATION = "DISPLAY_CURRENT_LOCATION";
    private static final String LOCATION_LATITUDE = "LOCATION_LATITUDE";
    private static final String LOCATION_LONGITUDE = "LOCATION_LONGITUDE";

    // A copy of the forecast API service
    private ForecastApi forecastApi;
    // A copy of the geolocation API service
    private GeolocationApi geolocationApi;
    // A copy of the forecast service
    private ForecastService forecastService = null;
    // Stores a copy of the shared event bus
    private ForecastBus eventBus;
    // A copy of the last forcast that we have for event bus production
    private Forecast lastForecast;
    // True if we're displaying the current locations weather
    private boolean isDisplayingCurrentLocation = true;
    // The name of the location we are displaying
    private String locationName;
    // Latitude and longitude of the location we're displaying
    private String locationLatitude;
    private String locationLongitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        // Setup the view pager
        ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new MainFragmentPagerAdapter());
        viewPager.setPageMargin(10);
        viewPager.setPageMarginDrawable(R.color.between_pages);
        // Bind the tab indicator
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
        tabs.setViewPager(viewPager);

        // Get the shared event bus
        eventBus = ForecastBus.get();
        // Get the forecast service
        forecastApi = ForecastApiBuilder.get();
        // Get the geolocation service
        geolocationApi = GeolocationApiBuilder.get();

        // Restore the state
        if (savedInstanceState != null) {
            isDisplayingCurrentLocation = savedInstanceState.getBoolean(DISPLAY_CURRENT_LOCATION, true);
            locationLatitude = savedInstanceState.getString(LOCATION_LATITUDE, null);
            locationLongitude = savedInstanceState.getString(LOCATION_LONGITUDE, null);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Register on the bus
        eventBus.register(this);

        // Start the location service if we're displaying the current location
        if (isDisplayingCurrentLocation) {
            // Bind to the forecast service
            Intent forecastIntent = new Intent(HomeActivity.this, ForecastService.class);
            bindService(forecastIntent, HomeActivity.this, BIND_AUTO_CREATE);
        }
        else {
            geolocationApi.search(GEOCODER_USERNAME, "Rusholme", 10, "MEDIUM", new LocationSearchCallback());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        // Unregister on the bus
        eventBus.unregister(this);
        // Unbind from the forecast service
        unbindService(this);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putBoolean(DISPLAY_CURRENT_LOCATION, isDisplayingCurrentLocation);
        outState.putString(LOCATION_LATITUDE, locationLatitude);
        outState.putString(LOCATION_LONGITUDE, locationLongitude);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.home, menu);

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

        return true;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        forecastService = ((ForecastBinder) service).getService();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        forecastService = null;
    }

    // TODO: Only make an API request if we havn't already made one close already
    @Subscribe
    public void onLocationChanged(LocationAvailableEvent locationEvent) {
        // Get the location from the event
        Location location = locationEvent.getLocation();

        String latitude = "" + location.getLatitude();
        String longitude = "" + location.getLongitude();

        // Make the API call to get the weather
        startForecastCall(latitude, longitude);
        // Make the API request to get the location name
        geolocationApi.locate(GEOCODER_USERNAME, latitude, longitude, new GeolocationCallback());
    }

    // TODO: Include a button to fix for devices which can support it
    @Subscribe
    public void onPlayServicesError(PlayServicesErrorEvent playServicesErrorEvent) {
        Log.w("Forecast", "Google Play Services not installed");
        Intent intent = new Intent(this, PlayServicesErrorActivity.class);
        startActivity(intent);
    }

    @Produce
    public ForecastAvailableEvent produceLastLocation() {
        if (lastForecast != null) {
            return new ForecastAvailableEvent(lastForecast);
        }
        else {
            return null;
        }
    }

    private void startForecastCall(String latitude, String longitude) {
        // Get the latitude and longitude
        locationLatitude = latitude;
        locationLongitude = longitude;

        forecastApi.forecast(API_KEY, locationLatitude, locationLongitude, "auto", new ForecastCallback());
    }

    private class ForecastCallback implements Callback<Forecast> {
        @Override
        public void success(Forecast forecast, Response responce) {
            // Post the new forecast on the event bus
            lastForecast = forecast;
            eventBus.post(new ForecastAvailableEvent(forecast));
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO: Handle and show this error
            Log.d("ws.oakes.forecast.api.Forecast", "Forecast Failed: " + error);
        }
    }

    private class GeolocationCallback implements Callback<Geolocations> {
        @Override
        public void success(Geolocations geolocations, Response responce) {
            // Check we have been returned a valid location
            if (geolocations.geonames != null && geolocations.geonames.size() > 0) {
                // Get the location data
                Geolocation location = geolocations.geonames.get(0);
                String placeName = location.name;
                String countryName = location.countryName;

                // Set the action bar title
                ActionBar actionBar = getActionBar();
                actionBar.setSubtitle(placeName + ", " + countryName);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO: Handle and show this error
            Log.d("ws.oakes.forecast.api.Forecast", "Geolocation Failed: " + error);
        }
    }

    private class LocationSearchCallback implements Callback<SearchLocations> {
        @Override
        public void success(SearchLocations locations, Response responce) {
            if (locations.geonames != null && locations.geonames.size() > 0) {
                // Get the location data
                SearchLocation location = locations.geonames.get(0);
                String placeName = location.name;
                String countryName = location.countryName;

                // Set the action bar title
                ActionBar actionBar = getActionBar();
                actionBar.setSubtitle(placeName + ", " + countryName);

                // Make the API call to get the weather
                startForecastCall(location.lat, location.lng);
            }
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO: Handle and show this error
            Log.d("ws.oakes.forecast.api.Forecast", "Search Failed: " + error);
        }
    }

    private class MainFragmentPagerAdapter extends FragmentPagerAdapter {
        private final static int PAGE_COUNT = 4;
        private String[] titles;
 
        public MainFragmentPagerAdapter() {
            super(getSupportFragmentManager());

            Resources res = getResources();
            titles = res.getStringArray(R.array.main_tab_titles);
        }
 
        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }
 
        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SummaryFragment();
                case 1:
                    return new NextWeekFragment();
                case 2:
                    return new NextDayFragment();
                case 3:
                    return new NextHourFragment();
                default:
                    // This can never happen
                    Log.wtf("Forecast", "Creating a fragment in a position which souldn't exists...");
                    return new Fragment();
            }
        }
    }
}
