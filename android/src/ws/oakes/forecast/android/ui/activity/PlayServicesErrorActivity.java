package ws.oakes.forecast.android.ui.activity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import ws.oakes.forecast.android.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;

public class PlayServicesErrorActivity extends Activity implements
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    // The request code used when requesting a resolution
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;
    // Stores the current instantiation of the location client in this object
    private LocationClient locationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_services_error);

        // Create a new location clientw
        locationClient = new LocationClient(this, this, this);

        if (checkPlayServicesConnection()) {
            locationClient.connect();
        }
    }

    @Override
    protected void onDestroy() {
        locationClient.disconnect();
        super.onDestroy();
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        finish();
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        /*
         * Google Play services can resolve some errors it detects.
         * If the error has a resolution, try sending an Intent to
         * start a Google Play services activity that can resolve
         * error.
         */
        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            }
            // Thrown if Google Play services canceled the original PendingIntent
            catch (IntentSender.SendIntentException e) {
                // Log the error
                e.printStackTrace();
            }
        }
        else {
            // TODO: If no resolution is available, display a dialog to the user with the error.
            // showErrorDialog(connectionResult.getErrorCode());
        }
    }

    /*
     * Handle results returned to this Activity by other Activities started with
     * startActivityForResult(). In particular, the method onConnectionFailed() in
     * LocationUpdateRemover and LocationUpdateRequester may call startResolutionForResult() to
     * start an Activity that handles Google Play services problems. The result of this
     * call returns here, to onActivityResult.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        // If the request code matches the code sent in onConnectionFailed
        if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            switch (resultCode) {
                // If Google Play services resolved the problem
                case Activity.RESULT_OK:
                    Log.d("Forecast", "A location problem has been resolved");
                    finish();
                break;

                // If any other result was returned by Google Play services
                default:
                    Log.d("Forecast", "There was no resolution");
                break;
            }
        }
        // It's a unknown request code, error
        else {
            Log.d("Forecast", "Unknown request code: " + requestCode);
        }
    }

    /**
     * Called when the user presses the fixed button in the UI
     **/
    public void fixed(View v) {
        finish();
    }

    /**
     * Checks if the play services are connected and displays an error if not
     **/
    private boolean checkPlayServicesConnection() {
        // Check if we have the Google Play Services available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode == ConnectionResult.SUCCESS) {
            // All good to go
            return true;
        }
        else if (resultCode == ConnectionResult.SERVICE_MISSING ||
                resultCode == ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED ||
                resultCode == ConnectionResult.SERVICE_DISABLED) {
            // Show the error dialog when they are not available
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(resultCode, this, 1);
            dialog.show();
        }
        else {
            // Show an unknown error when something silly has happened
            AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
            builder.setMessage(R.string.play_services_unknown_error);
            builder.setCancelable(true);
            builder.setPositiveButton(getString(R.string.play_services_unknown_button), null);
            AlertDialog dialog = builder.create();
            dialog.show();
        }

        return false;
    }
}
