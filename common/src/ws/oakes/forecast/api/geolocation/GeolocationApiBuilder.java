package ws.oakes.forecast.common.api.geolocation;

import retrofit.RestAdapter;

public class GeolocationApiBuilder {
    private static GeolocationApi geolocationService = null;

    public static GeolocationApi get() {
        if (geolocationService == null) {
            // Setup the REST client
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setServer("http://api.geonames.org") // The base API endpoint.
                    .build();
            restAdapter.setDebug(true);
            geolocationService = restAdapter.create(GeolocationApi.class);
        }

        return geolocationService;
    }
}
