package ws.oakes.forecast.common.api.geolocation;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

import java.util.List;

public interface GeolocationApi {
    @GET("/findNearbyPlaceNameJSON")
    void locate(
            @Query("username") String apikey,
            @Query("lat") String lat,
            @Query("lng") String lon,
            Callback<Geolocations> callback
    );

    @GET("/searchJSON")
    void search(
            @Query("username") String apikey,
            @Query("q") String q,
            @Query("maxRows") int maxRows,
            @Query("style") String style,
            Callback<SearchLocations> callback
    );
}
