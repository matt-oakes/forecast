package ws.oakes.forecast.common.api.geolocation;

public class SearchLocation {
	public String name;
	public String countryCode;
	public String countryName;
	public String lng;
	public String lat;

	public String toString() {
		if (name != null && countryName != null) {
			return name + ", " + countryName;
		}
		else {
			return "Unknown Location";
		}
	}
}