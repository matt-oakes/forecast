package ws.oakes.forecast.common.api;

public class ForecastDataPoint {
    public Long time;
    public String summary;
    public String icon;
    public String sunriseTime;
    public String sunsetTime;
    public Float precipIntensity;
    public Float precipIntensityMax;
    public Float precipIntensityMin;
    public Float precipProbability;
    public String precipType;
    public String precipAccumulation;
    public Float temperature;
    public Float temperatureMin;
    public Long temperatureMinTime;
    public Float temperatureMax;
    public Long temperatureMaxTime;
    public Float dewPoint;
    public Float windSpeed;
    public Float windBearing;
    public Float cloudCover;
    public Float humidity;
    public Float pressure;
    public Float visibility;
    public Float ozone;
}
