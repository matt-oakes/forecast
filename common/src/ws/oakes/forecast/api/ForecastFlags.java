package ws.oakes.forecast.common.api;

import com.google.gson.annotations.SerializedName;

public class ForecastFlags {
	public static final String SI_TEMPERATURE_UNITS = "si";
	public static final String UK_TEMPERATURE_UNITS = "uk";
	public static final String CA_TEMPERATURE_UNITS = "ca";
	public static final String US_TEMPERATURE_UNITS = "us";

	@SerializedName("darksky-unavailable") public String darkskyUnavailable;
	@SerializedName("darksky-stations") public String[] darkskyStations;
	@SerializedName("datapoint-stations") public String[] datapointStations;
	@SerializedName("isd-stations") public String[] isdStations;
	@SerializedName("lamp-stations") public String[] lampStations;
	@SerializedName("metar-stations") public String[] metarStations;
	@SerializedName("metno-license") public String metnoLicense;
	public String[] sources;
	public String units;
}