package ws.oakes.forecast.common.api;

import java.util.List;

public class ForecastDataBlock {
    public String summary;
    public long time;
    public List<ForecastDataPoint> data;
}
