package ws.oakes.forecast.common.api;

public class ForecastAlert {
    public String title;
    public long expires;
    public String uri;
}
