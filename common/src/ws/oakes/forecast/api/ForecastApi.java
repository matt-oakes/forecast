package ws.oakes.forecast.common.api;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface ForecastApi {
    @GET("/forecast/{api_key}/{lat},{lon}")
    void forecast(
            @Path("api_key") String apikey,
            @Path("lat") String lat,
            @Path("lon") String lon,
            @Query("units") String units,
            Callback<Forecast> callback
    );

    @GET("/forecast/{api_key}/{lat},{lon},{time}")
    void forecastAtTime(
            @Path("api_key") String apikey,
            @Path("lat") String lat,
            @Path("lon") String lon,
            @Path("time") String time,
            @Query("units") String units,
            Callback<Forecast> callback
    );
}
