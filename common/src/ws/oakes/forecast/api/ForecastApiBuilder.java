package ws.oakes.forecast.common.api;

import retrofit.RestAdapter;

public class ForecastApiBuilder {
    private static ForecastApi mForecastService = null;

    public static ForecastApi get() {
        if (mForecastService == null) {
            // Setup the REST client
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setServer("https://api.forecast.io") // The base API endpoint.
                    .build();
            mForecastService = restAdapter.create(ForecastApi.class);
        }

        return mForecastService;
    }
}
