package ws.oakes.forecast.common.api;

import java.util.List;

public class Forecast {
    public String latitude;
    public String longitude;
    public String timezone;
    public int offset;
    public ForecastDataPoint currently;
    public ForecastDataBlock minutely;
    public ForecastDataBlock hourly;
    public ForecastDataBlock daily;
    public List<ForecastAlert> alerts;
    public ForecastFlags flags;
}
