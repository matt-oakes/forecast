package ws.oakes.forecast.common.util;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;

import com.squareup.otto.Produce;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class ForecastService extends Service implements
        LocationListener,
        GooglePlayServicesClient.ConnectionCallbacks,
        GooglePlayServicesClient.OnConnectionFailedListener {

    // This is the object that receives interactions from clients.  See
    // RemoteService for a more complete example.
    private final IBinder binder = new ForecastBinder();

    // The location update times in ms
    private static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = 60 * 1000;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10 * 60 * 1000;

    // A request to connect to Location Services
    private LocationRequest locationRequest;
    // Stores the current instantiation of the location client in this object
    private LocationClient locationClient;
    // Stores a copy of the shared event bus
    private ForecastBus eventBus;

    // Keeps hold of the last location known to give when a new subscriber arrives
    private Location lastLocation;

    public class ForecastBinder extends Binder {
        public ForecastService getService() {
            return ForecastService.this;
        }
    }

    @Override
    public void onCreate() {
        // Create a new global location parameters object
        locationRequest = LocationRequest.create();
        // Use high accuracy
        locationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        // Set the update interval
        locationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // Set the interval ceiling to one minute
        locationRequest.setFastestInterval(FAST_INTERVAL_CEILING_IN_MILLISECONDS);

        // Create a new location clientw
        locationClient = new LocationClient(this, this, this);

        // Get the shared event bus and register as a producer
        eventBus = ForecastBus.get();
        eventBus.register(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // Unregister on the event bus
        eventBus.unregister(this);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Start the location service
        if (!locationClient.isConnected()) {
            locationClient.connect();
        }

        return binder;
    }

    @Override
    public boolean onUnbind(Intent intent) {
        // All clients have unbound with unbindService()

        // If the location client is connected
        if (locationClient.isConnected()) {
            stopPeriodicUpdates();
        }

        // After disconnect() is called, the client is considered dead
        locationClient.disconnect();

        return true;
    }

    /*
     * Called by Location Services when the request to connect the
     * client finishes successfully. At this point, you can
     * request the current location or start periodic updates
     */
    @Override
    public void onConnected(Bundle bundle) {
        if (checkPlayServicesConnection()) {
            // Start checking for location updates
            startPeriodicUpdates();
            // Get the current location
            Location currentLocation = locationClient.getLastLocation();
            handleNewLocation(currentLocation);
        }
    }

    /*
     * Called by Location Services if the connection to the
     * location client drops because of an error.
     */
    @Override
    public void onDisconnected() {
    }

    /*
     * Called by Location Services if the attempt to
     * Location Services fails.
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        sendPlayServicesError();
    }

    /**
     * Receive new locations from the play services
     *
     * @param location The updated location.
     */
    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }

    @Produce
    public LocationAvailableEvent produceLastLocation() {
        Log.d("Forecast", "Producing event");
        if (lastLocation != null) {
            return new LocationAvailableEvent(lastLocation);
        }
        else {
            return null;
        }
    }

    /**
     * In response to a request to start updates, send a request
     * to Location Services
     */
    private void startPeriodicUpdates() {
        locationClient.requestLocationUpdates(locationRequest, this);
    }

    /**
     * In response to a request to stop updates, send a request to
     * Location Services
     */
    private void stopPeriodicUpdates() {
        locationClient.removeLocationUpdates(this);
    }

    /**
     * Handles new location events
     **/
    private void handleNewLocation(Location location) {
        if (location != null) {
            // Keep a copy of the location here for later
            lastLocation = location;

            // Post the new location out onto the event bus
            Log.d("Forecast", "New location is: " + location.getLatitude() + " - " + location.getLongitude());
            eventBus.post(new LocationAvailableEvent(location));
        }
        else {
            Log.d("Forecast", "New location is null");
        }
    }

    /**
     * Checks if the play services are connected and displays an error if not
     **/
    private boolean checkPlayServicesConnection() {
        // Check if we have the Google Play Services available
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());
        if (resultCode == ConnectionResult.SUCCESS) {
            // All good to go
            return true;
        }
        else {
            // There's an error that we need to be handled
            sendPlayServicesError();
        }

        return false;
    }

    private void sendPlayServicesError() {
        eventBus.post(new PlayServicesErrorEvent());
    }
}
