package ws.oakes.forecast.common.util;

import android.location.Location;

public class LocationAvailableEvent {
    private Location location;

    public LocationAvailableEvent(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return location;
    }
}
