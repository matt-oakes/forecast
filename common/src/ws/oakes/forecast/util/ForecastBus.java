package ws.oakes.forecast.common.util;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

public class ForecastBus extends Bus {
	private static final ForecastBus bus = new ForecastBus();

	public static ForecastBus get() {
		return bus;
	}
}