package ws.oakes.forecast.common.util;

import ws.oakes.forecast.common.R;
import ws.oakes.forecast.common.api.ForecastFlags;

import android.content.Context;
import android.content.res.Resources;

import java.util.HashMap;
import java.util.Map;

public class ForecastUtils {
	public static final Map<String, Integer> iconMap = new HashMap<String, Integer>();

	// Populate the icon map
	static {
		iconMap.put("clear-day", R.drawable.clear_day);
		iconMap.put("clear-night", R.drawable.clear_night);
		iconMap.put("rain", R.drawable.rain);
		iconMap.put("snow", R.drawable.snow);
		iconMap.put("sleet", R.drawable.sleet);
		iconMap.put("wind", R.drawable.wind);
		iconMap.put("fog", R.drawable.fog);
		iconMap.put("cloudy", R.drawable.cloudy);
		iconMap.put("partly-cloudy-day", R.drawable.partly_cloudy_day);
		iconMap.put("partly-cloudy-night", R.drawable.partly_cloudy_night);
		iconMap.put("hail", R.drawable.hail);
		iconMap.put("thunderstorm", R.drawable.thunderstorm);
	}

	public static String getTemperatureString(Context context, float temperature, String units) {
		return getTemperatureString(context, Math.round(temperature), units);
	}

	public static String getTemperatureString(Context context, int temperature, String units) {
		Resources res = context.getResources();
		// Get the string format
		String currentTemperatureString = res.getString(R.string.current_temperature);

		// Choose the correct temperature units
        String unitString;
        if (units.equals(ForecastFlags.US_TEMPERATURE_UNITS)) {
            unitString = res.getString(R.string.temperature_f);
        }
        else {
            unitString = res.getString(R.string.temperature_c);
        }

        // Return the formatted string
        return String.format(currentTemperatureString, temperature, unitString);
	}
}