package ws.oakes.forecast.common.util;

import ws.oakes.forecast.common.api.Forecast;

public class ForecastAvailableEvent {
    private Forecast forecast;

    public ForecastAvailableEvent(Forecast forecast) {
        this.forecast = forecast;
    }

    public Forecast getForecast() {
        return forecast;
    }
}
