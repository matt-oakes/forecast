package ws.oakes.forecast.ouya;

import java.util.HashMap;
import java.util.Map;

public class ImageUtils {
	public static final String CLEAR_DAY = "clear-day";
	public static final String CLEAR_NIGHT = "clear-night";
	public static final String RAIN = "rain";
	public static final String SNOW = "snow";
	public static final String SLEET = "sleet";
	public static final String WIND = "wind";
	public static final String FOG = "fog";
	public static final String CLOUDY = "cloudy";
	public static final String PARTLY_CLOUDY_DAY = "partly-cloudy-day";
	public static final String PARTLY_CLOUDY_NIGHT = "partly-cloudy-night";
	public static final String HAIL = "hail";
	public static final String THUNDERSTORM = "thunderstorm";

	// The map of names to icons
    public static final Map<String, Integer> iconMap = new HashMap<String, Integer>();
    // Populate the icon map
    static {
        iconMap.put(CLEAR_DAY, R.drawable.clear_day);
        iconMap.put(CLEAR_NIGHT, R.drawable.clear_night);
        iconMap.put(RAIN, R.drawable.rain);
        iconMap.put(SNOW, R.drawable.snow);
        iconMap.put(SLEET, R.drawable.sleet);
        iconMap.put(WIND, R.drawable.wind);
        iconMap.put(FOG, R.drawable.fog);
        iconMap.put(CLOUDY, R.drawable.cloudy);
        iconMap.put(PARTLY_CLOUDY_DAY, R.drawable.partly_cloudy_day);
        iconMap.put(PARTLY_CLOUDY_NIGHT, R.drawable.partly_cloudy_night);
        iconMap.put(HAIL, R.drawable.hail);
        iconMap.put(THUNDERSTORM, R.drawable.thunderstorm);
    }

    // The map of names to background
    public static final Map<String, Integer> backgroundMap = new HashMap<String, Integer>();
    // Populate the background map
    static {
        backgroundMap.put(CLEAR_DAY, R.drawable.background_clear_day);
        backgroundMap.put(CLEAR_NIGHT, R.drawable.background_clear_night);
        backgroundMap.put(RAIN, R.drawable.background_rain);
        backgroundMap.put(SNOW, R.drawable.background_snow);
        backgroundMap.put(SLEET, R.drawable.background_sleet);
        backgroundMap.put(WIND, R.drawable.background_wind);
        backgroundMap.put(FOG, R.drawable.background_fog);
        backgroundMap.put(CLOUDY, R.drawable.background_cloudy);
        backgroundMap.put(PARTLY_CLOUDY_DAY, R.drawable.background_partly_cloudy_day);
        backgroundMap.put(PARTLY_CLOUDY_NIGHT, R.drawable.background_partly_cloudy_night);
        backgroundMap.put(HAIL, R.drawable.background_hail);
        backgroundMap.put(THUNDERSTORM, R.drawable.background_thunderstorm);
    }

    // The map of names to background
    public static final Map<String, String> creditTextMap = new HashMap<String, String>();
    // Populate the background map
    static {
        creditTextMap.put(CLEAR_DAY, "Black winter flowers by Wonderlane");
        creditTextMap.put(CLEAR_NIGHT, "Sycamore Gap (At Night) by wazimu0");
        creditTextMap.put(RAIN, "Rain by Jon Shave");
        creditTextMap.put(SNOW, "Snow by CmdrGravy");
        creditTextMap.put(SLEET, "Snow by CmdrGravy");
        creditTextMap.put(WIND, "Windy Day by Daniel R. Blume");
        creditTextMap.put(FOG, "Fog on the Bay by Frankzed");
        creditTextMap.put(CLOUDY, "Cloudy day panorama by Luis Argerich");
        creditTextMap.put(PARTLY_CLOUDY_DAY, "Cloudy Weather by Wouter");
        creditTextMap.put(PARTLY_CLOUDY_NIGHT, "Moonlit cloudy night by baronsquirrel");
        creditTextMap.put(HAIL, "Hailed! by Ian Boggs");
        creditTextMap.put(THUNDERSTORM, "Thunderstorm rolls through by Not So Nice Duck");
    }

    // The map of names to background
    public static final Map<String, String> creditUrlMap = new HashMap<String, String>();
    // Populate the background map
    static {
        creditUrlMap.put(CLEAR_DAY, "http://flic.kr/p/7CsKbP");
        creditUrlMap.put(CLEAR_NIGHT, "http://flic.kr/p/dc459q");
        creditUrlMap.put(RAIN, "http://flic.kr/p/4WAPfj");
        creditUrlMap.put(SNOW, "http://flic.kr/p/azDQa");
        creditUrlMap.put(SLEET, "http://flic.kr/p/azDQa");
        creditUrlMap.put(WIND, "http://flic.kr/p/5y38dy");
        creditUrlMap.put(FOG, "http://flic.kr/p/9XzhJ8");
        creditUrlMap.put(CLOUDY, "http://flic.kr/p/5BUhYR");
        creditUrlMap.put(PARTLY_CLOUDY_DAY, "http://flic.kr/p/24uq2s");
        creditUrlMap.put(PARTLY_CLOUDY_NIGHT, "http://flic.kr/p/ap1y2");
        creditUrlMap.put(HAIL, "http://flic.kr/p/SAtA");
        creditUrlMap.put(THUNDERSTORM, "http://flic.kr/p/4EU1BZ");
    }
}