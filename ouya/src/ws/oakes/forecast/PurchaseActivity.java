package ws.oakes.forecast.ouya;

import tv.ouya.console.api.CancelIgnoringOuyaResponseListener;
import tv.ouya.console.api.OuyaAuthenticationHelper;
import tv.ouya.console.api.OuyaController;
import tv.ouya.console.api.OuyaFacade;
import tv.ouya.console.api.OuyaEncryptionHelper;
import tv.ouya.console.api.OuyaResponseListener;
import tv.ouya.console.api.Product;
import tv.ouya.console.api.Purchasable;
import tv.ouya.console.api.Receipt;

import com.squareup.otto.Subscribe;

import us.feras.ecogallery.EcoGalleryAdapterView;

import ws.oakes.ouya.widget.OuyaGalleryView;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastUtils;

import com.bugsense.trace.BugSenseHandler;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.BaseAdapter;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONObject;
import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.Map;

public class PurchaseActivity extends Activity implements OuyaGalleryView.OnItemClickListener {
    public static final String PRODUCT_1 = "full_foresight_1";
    public static final String PRODUCT_2 = "full_foresight_2";
    public static final String PRODUCT_4 = "full_forecast";
    public static final String PRODUCT_6 = "full_foresight_6";
    public static final String PRODUCT_8 = "full_foresight_8";
    public static final String PRODUCT_10 = "full_foresight_10";
    public static final String PRODUCT_15 = "full_foresight_15";

    public static final List<String> PRODUCT_IDS = new ArrayList<String>();
    static {
        PRODUCT_IDS.add(PRODUCT_1); // $0.99
        PRODUCT_IDS.add(PRODUCT_2); // $1.99
        PRODUCT_IDS.add(PRODUCT_4); // $3.99
        PRODUCT_IDS.add(PRODUCT_6); // $5.99
        PRODUCT_IDS.add(PRODUCT_8); // $7.99
        PRODUCT_IDS.add(PRODUCT_10); // $9.99
        PRODUCT_IDS.add(PRODUCT_15); // $14.99
    }
    public static final Map<String, String> PRODUCT_PRICE = new HashMap<String, String>();
    static {
        PRODUCT_PRICE.put(PRODUCT_1, "$0.99");
        PRODUCT_PRICE.put(PRODUCT_2, "$1.99");
        PRODUCT_PRICE.put(PRODUCT_4, "$3.99");
        PRODUCT_PRICE.put(PRODUCT_6, "$5.99");
        PRODUCT_PRICE.put(PRODUCT_8, "$7.99");
        PRODUCT_PRICE.put(PRODUCT_10, "$9.99");
        PRODUCT_PRICE.put(PRODUCT_15, "$14.99");
    }

	public static final Paint greyscalePaint = new Paint();
	static {
		ColorMatrix matrix = new ColorMatrix();
	    matrix.setSaturation(0);
	    greyscalePaint.setColorFilter(new ColorMatrixColorFilter(matrix));
	}

    public OuyaGalleryView listView;
    public ProductAdapter adapter;
    public boolean firstNoGreyscaleDone = false;

    private PublicKey publicKey;
    private Map<String, String> outstandingPurchaseRequests = new HashMap<String, String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.purchase);
        BugSenseHandler.initAndStartSession(PurchaseActivity.this, HomeActivity.BUGSENSE_API_KEY);

        OuyaFacade.getInstance().init(this, HomeActivity.OUYA_UUID);

        // Change the dim amount of the background
        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
        windowManager.dimAmount = 0.75f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

        // Get the views
        listView = (OuyaGalleryView) findViewById(R.id.list);
        listView.setAdapter(new ProductAdapter());
        listView.setVisibility(View.INVISIBLE);

        // Hacks to select the right option
        Handler myHandler = new Handler();
        myHandler.postDelayed(selectionRunable, 50);
        myHandler.postDelayed(selectionRunable, 200);
        myHandler.postDelayed(showListRunable, 600);

        // Setup the change listener for greyscale
        listView.setOnItemSelectedListener(new SelectionChangeListener());
        listView.setOnItemClickListener(this);

        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(HomeActivity.APPLICATION_KEY);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
        }
        catch (Exception e) {
            Log.e("Forecast", "Unable to make public key", e);
            Toast.makeText(PurchaseActivity.this, "Error creating public application key. Try again or contact the developer.", Toast.LENGTH_LONG).show();
            finish();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BugSenseHandler.closeSession(PurchaseActivity.this);
        OuyaFacade.getInstance().shutdown();
    }

    @Override
    public void onItemClick(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
        String productId = PRODUCT_IDS.get(position);
        purchase(productId);
    }

    private void completedPurchase() {
        setResult(RESULT_OK);
        finish();
    }

    private Runnable selectionRunable = new Runnable() {
        @Override
        public void run() {
            listView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_DOWN, KeyEvent.KEYCODE_DPAD_RIGHT));
            listView.dispatchKeyEvent(new KeyEvent(KeyEvent.ACTION_UP, KeyEvent.KEYCODE_DPAD_RIGHT));
        }
    };
    private Runnable showListRunable = new Runnable() {
        @Override
        public void run() {
            listView.setVisibility(View.VISIBLE);
        }
    };

    public class ProductAdapter extends BaseAdapter {
    	private LayoutInflater inflater;

    	public ProductAdapter() {
    		super();
    		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	}

    	@Override
        public int getCount() {
            return PRODUCT_IDS.size();
        }

        @Override
        public Object getItem(int position) {
            return PRODUCT_IDS.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_product, parent, false);

                holder = new Holder();
                holder.card = convertView.findViewById(R.id.card);
                holder.price = (TextView) convertView.findViewById(R.id.price);

                convertView.setTag(holder);
            }
            else {
                holder = (Holder) convertView.getTag();
            }

            String product = PRODUCT_IDS.get(position);
            holder.price.setText(PRODUCT_PRICE.get(product));

            if (position != 0) { // NOTE: This is a hack :/ It avoids the first item being greyscale on startup
	            convertView.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
            }

            return convertView;
        }

        public class Holder {
        	View card;
            TextView price;
        }
    }

    public class SelectionChangeListener implements OuyaGalleryView.OnItemSelectedListener {
    	private View prevSelected = null;

    	@Override
    	public void onItemSelected(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
    		// Reset the previously selected
    		if (prevSelected != null) {
    			prevSelected.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
    		}

    		// Set the new view to non-greyscale and store a copy for later
			view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			prevSelected = view;
    	}

    	@Override
		public void onNothingSelected(EcoGalleryAdapterView<?> parent) {
		}
    }

    public void purchase(String productId) {
        OuyaFacade ouyaFacade = OuyaFacade.getInstance();
        if (ouyaFacade.isRunningOnOUYAHardware()) {
            try {
                requestPurchase(productId);
            } catch (Exception e) {
                Log.e("Forecast", "Unable to make a purchase.", e);
                Toast.makeText(PurchaseActivity.this, "Unable to purchase Foresight. Try again or contact the developer.", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void requestPurchase(final String product) throws GeneralSecurityException,
                                                               UnsupportedEncodingException,
                                                               JSONException {
        SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");

        // This is an ID that allows you to associate a successful purchase with
        // it's original request. The server does nothing with this string except
        // pass it back to you, so it only needs to be unique within this instance
        // of your app to allow you to pair responses with requests.
        String uniqueId = Long.toHexString(sr.nextLong());

        JSONObject purchaseRequest = new JSONObject();
        purchaseRequest.put("uuid", uniqueId);
        purchaseRequest.put("identifier", product);
        // This value is only needed for testing, not setting it results in a live purchase
        purchaseRequest.put("testing", "true"); 
        String purchaseRequestJson = purchaseRequest.toString();

        byte[] keyBytes = new byte[16];
        sr.nextBytes(keyBytes);
        SecretKey key = new SecretKeySpec(keyBytes, "AES");

        byte[] ivBytes = new byte[16];
        sr.nextBytes(ivBytes);
        IvParameterSpec iv = new IvParameterSpec(ivBytes);

        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, key, iv);
        byte[] payload = cipher.doFinal(purchaseRequestJson.getBytes("UTF-8"));

        cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding", "BC");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedKey = cipher.doFinal(keyBytes);

        Purchasable purchasable =
                new Purchasable(
                        product,
                        Base64.encodeToString(encryptedKey, Base64.NO_WRAP),
                        Base64.encodeToString(ivBytes, Base64.NO_WRAP),
                        Base64.encodeToString(payload, Base64.NO_WRAP) );

        synchronized (outstandingPurchaseRequests) {
            outstandingPurchaseRequests.put(uniqueId, product);
        }
        OuyaFacade.getInstance().requestPurchase(purchasable, new PurchaseListener(product));
    }

    private class PurchaseListener implements OuyaResponseListener<String> {
        public PurchaseListener(String productId) {
        }

        @Override
        public void onSuccess(String result) {
            completedPurchase();
        }

        @Override
        public void onFailure(int errorCode, String errorMessage, Bundle optionalData) {
             boolean wasHandledByAuthHelper =
                    OuyaAuthenticationHelper.
                            handleError(
                                    PurchaseActivity.this, errorCode, errorMessage,
                                    optionalData, 1,
                                    new ErrorListener());

            if (!wasHandledByAuthHelper) {
                Toast.makeText(PurchaseActivity.this, "Foresight purchase failed. Check your internet connection and try again. Contact the developer if this problem persists.", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onCancel() {
        }
    }

    private class ErrorListener implements OuyaResponseListener<Void> {
        @Override
        public void onSuccess(Void result) {
            Toast.makeText(PurchaseActivity.this, "Please attempt your purchase again, or contact the developer if this problem persists.", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onFailure(int errorCode, String errorMessage, Bundle optionalData) {
            Toast.makeText(PurchaseActivity.this, "Foresight purchase failed. Check your internet connection and try again. Contact the developer if this problem persists.", Toast.LENGTH_LONG).show();
        }

        @Override
        public void onCancel() {
        }
    }
}
