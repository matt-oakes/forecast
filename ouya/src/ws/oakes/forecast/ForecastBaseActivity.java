package ws.oakes.forecast.ouya;

import tv.ouya.console.api.OuyaController;

import com.squareup.otto.Subscribe;

import us.feras.ecogallery.EcoGalleryAdapterView;

import ws.oakes.ouya.widget.OuyaGalleryView;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastUtils;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class ForecastBaseActivity extends Activity {
	public static final Paint greyscalePaint = new Paint();
	static {
		ColorMatrix matrix = new ColorMatrix();
	    matrix.setSaturation(0);
	    greyscalePaint.setColorFilter(new ColorMatrixColorFilter(matrix));
	}

    public Forecast displayedForecast;
    public OuyaGalleryView listView;
    public ForecastAdapter adapter;
    public boolean firstNoGreyscaleDone = false;

    public void initForecastActivity() {
        // Change the dim amount of the background
        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
	    windowManager.dimAmount = 0.75f;
	    getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);

	    // Get the views
        listView = (OuyaGalleryView) findViewById(R.id.list);

        // Setup the change listener for greyscale
        listView.setOnItemSelectedListener(new SelectionChangeListener());
    }

    public void setupForecast(ForecastEvent forecastEvent) {
        firstNoGreyscaleDone = false;

        displayedForecast = forecastEvent.getForecast();
        List<ForecastDataPoint> data = getForecastData(displayedForecast);

        adapter = createAdapter(data);
        listView.setAdapter(adapter);
    }

    public abstract ForecastAdapter createAdapter(List<ForecastDataPoint> data);
    public abstract List<ForecastDataPoint> getForecastData(Forecast input);

    public abstract class ForecastAdapter extends BaseAdapter {
    	private List<ForecastDataPoint> days;
    	private LayoutInflater inflater;

    	public ForecastAdapter(List<ForecastDataPoint> days) {
    		super();
    		this.days = days;
    		inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	}

    	@Override
        public int getCount() {
            return days.size();
        }

        @Override
        public Object getItem(int position) {
            return days.get(position);
        }

        @Override
        public long getItemId(int position) {
            return days.get(position).time;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(getLayoutResource(), parent, false);

                holder = new Holder();
                holder.card = convertView.findViewById(R.id.card);
                holder.icon = (ImageView) convertView.findViewById(R.id.icon);
                holder.date = (TextView) convertView.findViewById(R.id.date);
                holder.summary = (TextView) convertView.findViewById(R.id.summary);
                holder.temperature = (TextView) convertView.findViewById(R.id.temperature);
                holder.maxTemperature = (TextView) convertView.findViewById(R.id.max_temperature);
                holder.minTemperature = (TextView) convertView.findViewById(R.id.min_temperature);

                convertView.setTag(holder);
            }
            else {
                holder = (Holder) convertView.getTag();
            }

            ForecastDataPoint data = days.get(position);

            setBackground(holder, data);
            setIcon(holder, data);
            setDateLabel(holder, data);
            setSummary(holder, data);
            setTemperatures(holder, data);

            if (position != 0) { // NOTE: This is a hack :/ It avoids the first item being greyscale on startup
	            convertView.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
            }

            return convertView;
        }

        public abstract int getLayoutResource();

        public void setBackground(Holder holder, ForecastDataPoint data) {
            // Set the background for the card
            if (data.icon != null && ImageUtils.backgroundMap.containsKey(data.icon)) {
                int backgroundDrawable = ImageUtils.backgroundMap.get(data.icon);
                holder.card.setBackgroundResource(backgroundDrawable);
            }
            else {
                // TODO: Should have a default
            }
        }

        public void setIcon(Holder holder, ForecastDataPoint data) {
            // Set the icon
            if (data.icon != null && ImageUtils.iconMap.containsKey(data.icon)) {
                int iconDrawable = ImageUtils.iconMap.get(data.icon);
                holder.icon.setImageResource(iconDrawable);
            }
            else {
                // TODO: Should have a default
            }
        }

        public abstract void setDateLabel(Holder holder, ForecastDataPoint data);

        public void setSummary(Holder holder, ForecastDataPoint data) {
            if (data.summary != null) {
                holder.summary.setText(data.summary);
                holder.summary.setVisibility(View.VISIBLE);
            }
            else {
                holder.summary.setVisibility(View.GONE);
            }
        }

        public void setTemperatures(Holder holder, ForecastDataPoint data) {
            if (holder.maxTemperature != null && holder.minTemperature != null && data.temperatureMax != null
                    && data.temperatureMin != null && displayedForecast.flags.units != null) {
                String maxTemperature = ForecastUtils.getTemperatureString(getApplicationContext(),
                                                                           data.temperatureMax,
                                                                           displayedForecast.flags.units);
                holder.maxTemperature.setText(maxTemperature);
                holder.maxTemperature.setVisibility(View.VISIBLE);

                String minTemperature = ForecastUtils.getTemperatureString(getApplicationContext(),
                                                                           data.temperatureMin,
                                                                           displayedForecast.flags.units);
                holder.minTemperature.setText(minTemperature);
                holder.minTemperature.setVisibility(View.VISIBLE);
            }
            else if (holder.maxTemperature != null && holder.minTemperature != null) {
                holder.maxTemperature.setVisibility(View.GONE);
                holder.minTemperature.setVisibility(View.GONE);
            }

            if (holder.temperature != null && data.temperature != null && displayedForecast.flags.units != null) {
                String temperature = ForecastUtils.getTemperatureString(getApplicationContext(),
                                                                        data.temperature,
                                                                        displayedForecast.flags.units);
                holder.temperature.setText(temperature);
                holder.temperature.setVisibility(View.VISIBLE);
            }
            else if (holder.temperature != null) {
                holder.temperature.setVisibility(View.GONE);
            }
        }

        public class Holder {
        	View card;
            ImageView icon;
            TextView date;
            TextView summary;
            TextView temperature;
            TextView maxTemperature;
            TextView minTemperature;
        }
    }

    public class SelectionChangeListener implements OuyaGalleryView.OnItemSelectedListener {
    	private View prevSelected = null;

    	@Override
    	public void onItemSelected(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
    		// Reset the previously selected
    		if (prevSelected != null) {
    			prevSelected.setLayerType(View.LAYER_TYPE_HARDWARE, greyscalePaint);
    		}

    		// Set the new view to non-greyscale and store a copy for later
			view.setLayerType(View.LAYER_TYPE_HARDWARE, null);
			prevSelected = view;
    	}

    	@Override
		public void onNothingSelected(EcoGalleryAdapterView<?> parent) {
		}
    }
}
