package ws.oakes.forecast.ouya;

import ws.oakes.forecast.common.api.Forecast;

public class ForecastEvent {
	private Forecast forecast;

    public ForecastEvent(Forecast forecast) {
        this.forecast = forecast;
    }

    public Forecast getForecast() {
        return forecast;
    }
}