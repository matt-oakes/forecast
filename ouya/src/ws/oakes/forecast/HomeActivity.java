package ws.oakes.forecast.ouya;

import tv.ouya.console.api.CancelIgnoringOuyaResponseListener;
import tv.ouya.console.api.OuyaAuthenticationHelper;
import tv.ouya.console.api.OuyaController;
import tv.ouya.console.api.OuyaFacade;
import tv.ouya.console.api.OuyaEncryptionHelper;
import tv.ouya.console.api.OuyaResponseListener;
import tv.ouya.console.api.Product;
import tv.ouya.console.api.Purchasable;
import tv.ouya.console.api.Receipt;

import com.google.gson.Gson;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastApi;
import ws.oakes.forecast.common.api.ForecastApiBuilder;
import ws.oakes.forecast.common.api.geolocation.SearchLocation;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastUtils;

import ws.oakes.ouya.widget.OuyaControllerButton;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bugsense.trace.BugSenseHandler;

import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.*;
import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.json.JSONObject;
import org.json.JSONException;

public class HomeActivity extends Activity implements View.OnFocusChangeListener {
    public static final String BUGSENSE_API_KEY = "183c1c8c";

    private static final String API_KEY = "2298603da4018bc4045e8d0df1cbb931";
    public static final String OUYA_UUID = "7198d57d-8b92-4684-9f07-0d09d5d7d6ee";

    private static final String PREFS_NAME = "prefs";
    private static final String LOCATION_DATA = "LOCATION_DATA";
    private static final String NUMBER_OF_FORECASTS = "NUMBER_OF_FORECASTS";

    private static final int MAX_FORECASTS = 10;

    public static final byte[] APPLICATION_KEY = {
        (byte)0x30, (byte) 0x81, (byte)0x9f, (byte)0x30, (byte)0x0d, (byte)0x06, (byte)0x09, (byte)0x2a,
        (byte)0x86, (byte)0x48, (byte)0x86, (byte)0xf7, (byte)0x0d, (byte)0x01, (byte)0x01, (byte)0x01,
        (byte)0x05, (byte) 0x00, (byte)0x03, (byte)0x81, (byte)0x8d, (byte)0x00, (byte)0x30, (byte)0x81,
        (byte)0x89, (byte)0x02, (byte)0x81, (byte)0x81, (byte)0x00, (byte)0xbd, (byte)0xb3, (byte)0x97,
        (byte)0x13, (byte) 0xb3, (byte)0x69, (byte)0x2c, (byte)0xc2, (byte)0x45, (byte)0x3e, (byte)0x6a,
        (byte)0xdb, (byte)0x0c, (byte)0x17, (byte)0x02, (byte)0xa7, (byte)0x9e, (byte)0xa3, (byte)0xb8,
        (byte)0x58, (byte) 0x75, (byte)0x70, (byte)0x27, (byte)0x4d, (byte)0x09, (byte)0x23, (byte)0x54,
        (byte)0xd2, (byte)0x4f, (byte)0x5a, (byte)0x74, (byte)0x30, (byte)0x7b, (byte)0x91, (byte)0x30,
        (byte)0x32, (byte) 0xe3, (byte)0xa6, (byte)0x91, (byte)0x3e, (byte)0xcf, (byte)0x83, (byte)0xa1,
        (byte)0x3b, (byte)0xb3, (byte)0xd6, (byte)0x63, (byte)0x47, (byte)0x41, (byte)0x9a, (byte)0x7b,
        (byte)0x72, (byte) 0x3c, (byte)0x26, (byte)0x47, (byte)0x0f, (byte)0xa9, (byte)0xc7, (byte)0x1d,
        (byte)0x8e, (byte)0xf0, (byte)0x43, (byte)0x2d, (byte)0xe4, (byte)0xb4, (byte)0x57, (byte)0xf1,
        (byte)0x23, (byte) 0xab, (byte)0xc0, (byte)0x04, (byte)0x0e, (byte)0x6a, (byte)0xd5, (byte)0x94,
        (byte)0x57, (byte)0x2a, (byte)0xf8, (byte)0xa5, (byte)0xb5, (byte)0x35, (byte)0x5e, (byte)0xa3,
        (byte)0xb9, (byte) 0x0f, (byte)0xbd, (byte)0xb3, (byte)0x8f, (byte)0xb6, (byte)0x91, (byte)0x36,
        (byte)0xc9, (byte)0x3c, (byte)0xfb, (byte)0xd5, (byte)0x86, (byte)0x70, (byte)0x9d, (byte)0x18,
        (byte)0xa7, (byte) 0x28, (byte)0x49, (byte)0xd9, (byte)0x88, (byte)0x91, (byte)0x75, (byte)0xcc,
        (byte)0xc1, (byte)0x96, (byte)0xdc, (byte)0xa9, (byte)0x50, (byte)0x60, (byte)0x86, (byte)0xaa,
        (byte)0x7b, (byte) 0x84, (byte)0xd3, (byte)0x17, (byte)0xdc, (byte)0xcc, (byte)0xf1, (byte)0x52,
        (byte)0xaa, (byte)0x1a, (byte)0x60, (byte)0x17, (byte)0xdf, (byte)0x02, (byte)0x03, (byte)0x01,
        (byte)0x00, (byte) 0x01
    };

    private ForecastBus eventBus;
    private ForecastApi forecastApi;

    private Forecast lastForecast;

    private TextView nextHourMenu;
    private TextView nextDayMenu;
    private TextView nextWeekMenu;
    private TextView nextHourSummary;
    private TextView nextDaySummary;
    private TextView nextWeekSummary;
    private TextView currentSummary;
    private TextView currentTemperature;
    private TextView locationTitle;
    private TextView creditText;
    private TextView noInternet;
    private TextView forecastNumber;
    private ImageView currentIcon;
    private ProgressBar progressView;
    private View contentContainer;
    private View rootView;
    private View trialVersion;
    private View trialExpired;
    private OuyaControllerButton selectMenuButton;
    private OuyaControllerButton locationMenuButton;
    private OuyaControllerButton exitMenuButton;

    private int numberOfForecasts;
    private boolean hasFullVersion = false;
    private PublicKey publicKey;
    private Map<String, String> outstandingPurchaseRequests = new HashMap<String, String>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        BugSenseHandler.initAndStartSession(HomeActivity.this, BUGSENSE_API_KEY);

        try {
            X509EncodedKeySpec keySpec = new X509EncodedKeySpec(APPLICATION_KEY);
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            publicKey = keyFactory.generatePublic(keySpec);
        }
        catch (Exception e) {
            Log.e("Forecast", "Unable to make public key", e);
            Toast.makeText(HomeActivity.this, "Error creating public application key. Try again or contact the developer.", Toast.LENGTH_LONG).show();
            finish();
        }

        nextHourMenu = (TextView) findViewById(R.id.hour_menu);
        nextDayMenu = (TextView) findViewById(R.id.day_menu);
        nextWeekMenu = (TextView) findViewById(R.id.week_menu);
        nextHourSummary = (TextView) findViewById(R.id.hour_summary);
        nextDaySummary = (TextView) findViewById(R.id.day_summary);
        nextWeekSummary = (TextView) findViewById(R.id.week_summary);
        currentSummary = (TextView) findViewById(R.id.current_summary);
        currentTemperature = (TextView) findViewById(R.id.current_temperature);
        locationTitle = (TextView) findViewById(R.id.location_title);
        creditText = (TextView) findViewById(R.id.credit);
        noInternet = (TextView) findViewById(R.id.no_internet);
        forecastNumber = (TextView) findViewById(R.id.forecast_number);
        currentIcon = (ImageView) findViewById(R.id.icon);
        progressView = (ProgressBar) findViewById(R.id.progress);
        contentContainer = findViewById(R.id.content);
        rootView = findViewById(R.id.root);
        trialVersion = findViewById(R.id.trial);
        trialExpired = findViewById(R.id.trial_expired);
        selectMenuButton = (OuyaControllerButton) findViewById(R.id.button_select);
        locationMenuButton = (OuyaControllerButton) findViewById(R.id.button_location);
        exitMenuButton = (OuyaControllerButton) findViewById(R.id.button_exit);

        eventBus = ForecastBus.get();
        eventBus.register(this);

        forecastApi = ForecastApiBuilder.get();

        OuyaFacade.getInstance().init(this, OUYA_UUID);
        OuyaController.init(this);

        nextHourMenu.setOnFocusChangeListener(this);
        nextDayMenu.setOnFocusChangeListener(this);
        nextWeekMenu.setOnFocusChangeListener(this);

        contentContainer.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
        noInternet.setVisibility(View.GONE);
        trialExpired.setVisibility(View.GONE);
        trialVersion.setVisibility(View.GONE);

        // Check if the full version has been purchased and start forecast if it has
        // Shows no internet on error
        OuyaFacade.getInstance().requestReceipts(receiptListListener);
    }

    private void continueStart() {
        Log.d("Forecast", "Continue start");

        SharedPreferences data = getSharedPreferences(PREFS_NAME, 0);

        numberOfForecasts = data.getInt(NUMBER_OF_FORECASTS, 0);
        if (numberOfForecasts < MAX_FORECASTS || hasFullVersion) {
            startForecast();
        }
        else {
            trialExpired.setVisibility(View.VISIBLE);
            locationMenuButton.setVisibility(View.GONE);

            trialExpired.requestFocus();
        }
    }

    @Override
    protected void onDestroy() {
        OuyaFacade.getInstance().shutdown();
        eventBus.unregister(this);
        BugSenseHandler.closeSession(HomeActivity.this);
        super.onDestroy();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (v.equals(nextHourMenu)) {
            nextHourSummary.setSelected(hasFocus);
        }
        else if (v.equals(nextDayMenu)) {
            nextDaySummary.setSelected(hasFocus);
        }
        else if (v.equals(nextWeekMenu)) {
            nextWeekSummary.setSelected(hasFocus);
        }
    }

    @Override
    public boolean onKeyDown(final int keyCode, KeyEvent event){
        String currentKey;

        switch (keyCode){
        case OuyaController.BUTTON_Y:
            if (locationMenuButton.getVisibility() == View.VISIBLE) {
                changeLocation();
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Subscribe
    public void onLocationChanged(LocationChangeEvent locationEvent) {
        // Get the location from the event
        SearchLocation location = locationEvent.getLocation();
        // Make the API call to get the weather
        startForecastCall(location);
        // Save the new location as game data for next time
        saveLocation(location);
    }

    @Produce
    public ForecastEvent produceForecast() {
        if (lastForecast != null) {
            return new ForecastEvent(lastForecast);
        }
        else {
            return null;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            hasFullVersion = true;
            startForecast();
        }
    }

    public void nextHourMenu(View button) {
        Intent intent = new Intent(this, NextHourActivity.class);
        startActivity(intent);
    }

    public void nextDayMenu(View button) {
        Intent intent = new Intent(this, NextDayActivity.class);
        startActivity(intent);
    }

    public void nextWeekMenu(View button) {
        Intent intent = new Intent(this, NextWeekActivity.class);
        startActivity(intent);
    }

    public void purchase(View button) {
        Intent intent = new Intent(this, PurchaseActivity.class);
        startActivityForResult(intent, 0);
    }

    private void startForecast() {
        updateTrailDisplay();
        locationMenuButton.setVisibility(View.VISIBLE);

        SharedPreferences data = getSharedPreferences(PREFS_NAME, 0);
        String locationJson = data.getString(LOCATION_DATA, "");
        if (!locationJson.equals("")) {
            SearchLocation location = new Gson().fromJson(locationJson, SearchLocation.class);
            startForecastCall(location);
        }
        else {
            changeLocation();
        }
    }

    private void startForecastCall(SearchLocation location) {
        // Set the correct visibility of views
        contentContainer.setVisibility(View.GONE);
        progressView.setVisibility(View.VISIBLE);

        // Get the latitude and longitude
        String latitude = "" + location.lat;
        String longitude = "" + location.lng;

    	// Make the forecast request
        // TODO: Check that we have both lat and long values
        forecastApi.forecast(API_KEY, latitude, longitude, "auto", new ForecastCallback());

        // Set the location title
        locationTitle.setText(location.toString());
    }

    private void changeLocation() {
        Intent intent = new Intent(this, LocationActivity.class);
        startActivity(intent);
    }

    private void saveLocation(SearchLocation location) {
        String locationJson = new Gson().toJson(location);

        SharedPreferences data = getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = data.edit();
        editor.putString(LOCATION_DATA, locationJson);
        editor.commit();
    }

    private void showNoInternet() {
        contentContainer.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);
        noInternet.setVisibility(View.VISIBLE);

        selectMenuButton.setVisibility(View.GONE);
        locationMenuButton.setVisibility(View.GONE);
    }

    private void updateTrailDisplay() {
        if (!hasFullVersion) {
            String trailProgress = getString(R.string.trial_progress);
            forecastNumber.setText(String.format(trailProgress, MAX_FORECASTS - numberOfForecasts, MAX_FORECASTS));

            trialVersion.setVisibility(View.VISIBLE);
            trialExpired.setVisibility(View.GONE);
        }
        else {
            trialVersion.setVisibility(View.GONE);
            trialExpired.setVisibility(View.GONE);
        }
    }

    private class ForecastCallback implements Callback<Forecast> {
        @Override
        public void success(Forecast forecast, Response responce) {
            lastForecast = forecast;

            if (forecast != null && (forecast.minutely != null || forecast.hourly != null || forecast.daily != null)) {
                numberOfForecasts++;
                SharedPreferences data = getSharedPreferences(PREFS_NAME, 0);
                SharedPreferences.Editor editor = data.edit();
                editor.putInt(NUMBER_OF_FORECASTS, numberOfForecasts);
                editor.commit();
                updateTrailDisplay();

                setMinutely(forecast);
                setHourly(forecast);
                setDaily(forecast);
                setCurrently(forecast);

                contentContainer.setVisibility(View.VISIBLE);
                progressView.setVisibility(View.GONE);
                noInternet.setVisibility(View.GONE);
                trialExpired.setVisibility(View.GONE);

                contentContainer.requestFocus(); // HACK: To force menu focus as reshown
            }
            else {
                // TODO: Display an error, no forecast here
            }
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO: Handle and show this error
            Log.d("ws.oakes.forecast.api.Forecast", "Failed: " + error);
            showNoInternet();
        }

        private void setMinutely(Forecast forecast) {
            if (forecast.minutely != null) {
                nextHourMenu.setVisibility(View.VISIBLE);

                if (forecast.minutely.summary != null) {
                    nextHourSummary.setText(forecast.minutely.summary);
                    nextHourSummary.setVisibility(View.VISIBLE);
                }
                else {
                    nextHourSummary.setVisibility(View.GONE);
                }
            }
            else {
                nextHourMenu.setVisibility(View.GONE);
                nextHourSummary.setVisibility(View.GONE);
            }
        }

        private void setHourly(Forecast forecast) {
            if (forecast.hourly != null) {
                nextDayMenu.setVisibility(View.VISIBLE);

                if (forecast.hourly.summary != null) {
                    nextDaySummary.setText(forecast.hourly.summary);
                    nextDaySummary.setVisibility(View.VISIBLE);
                }
                else {
                    nextDaySummary.setVisibility(View.GONE);
                }
            }
            else {
                nextDayMenu.setVisibility(View.GONE);
                nextDaySummary.setVisibility(View.GONE);
            }
        }

        private void setDaily(Forecast forecast) {
            if (forecast.daily != null) {
                nextWeekMenu.setVisibility(View.VISIBLE);

                if (forecast.daily.summary != null) {
                    nextWeekSummary.setText(forecast.daily.summary);
                    nextWeekSummary.setVisibility(View.VISIBLE);
                }
                else {
                    nextWeekSummary.setVisibility(View.GONE);
                }
            }
            else {
                nextWeekMenu.setVisibility(View.GONE);
                nextWeekSummary.setVisibility(View.GONE);
            }
        }

        private void setCurrently(Forecast forecast) {
            if (forecast.currently != null) {
                if (forecast.currently.summary != null) {
                    currentSummary.setText(forecast.currently.summary);
                    currentSummary.setVisibility(View.VISIBLE);
                }
                else {
                    currentSummary.setVisibility(View.GONE);
                }

                if (forecast.currently.temperature != null && forecast.flags.units != null) {
                    String temperature = ForecastUtils.getTemperatureString(HomeActivity.this, forecast.currently.temperature, forecast.flags.units);
                    currentTemperature.setText(temperature);
                    currentTemperature.setVisibility(View.VISIBLE);
                }
                else {
                    currentTemperature.setVisibility(View.GONE);
                }

                // Set the icon
                if (forecast.currently.icon != null) {
                    String iconId = forecast.currently.icon;

                    Integer iconResource = ImageUtils.iconMap.get(iconId);
                    if (iconResource != null) {
                        currentIcon.setImageResource(iconResource);
                    }
                    currentIcon.setVisibility(View.VISIBLE);

                    Integer backgroundResource = ImageUtils.backgroundMap.get(iconId);
                    if (backgroundResource != null) {
                        rootView.setBackgroundResource(backgroundResource);
                        creditText.setText(ImageUtils.creditTextMap.get(iconId) + "\n" + ImageUtils.creditUrlMap.get(iconId));
                    }
                }
                else {
                    // TODO: Should have a default icon and background
                }
            }
            else {
                currentSummary.setVisibility(View.GONE);
                currentTemperature.setVisibility(View.GONE);
                currentIcon.setVisibility(View.GONE);
            }
        }
    }

    CancelIgnoringOuyaResponseListener<String> receiptListListener =
        new CancelIgnoringOuyaResponseListener<String>() {
            @Override
            public void onSuccess(String receiptResponse) {
                OuyaEncryptionHelper helper = new OuyaEncryptionHelper();
                List<Receipt> receipts = null;
                try {
                    JSONObject response = new JSONObject(receiptResponse);
                    receipts = helper.decryptReceiptResponse(response, publicKey);
                } catch (Exception e) {
                    throw new RuntimeException(e);
                }

                hasFullVersion = false;
                for (Receipt r : receipts) {
                    if (PurchaseActivity.PRODUCT_IDS.contains(r.getIdentifier())) {
                        hasFullVersion = true;
                    }
                }

                continueStart();
            }

            @Override
            public void onFailure(int errorCode, String errorMessage, Bundle errorBundle) {
                Log.d("Forecast", errorMessage);
                showNoInternet();
            }
        };
}
