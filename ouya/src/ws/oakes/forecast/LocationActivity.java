package ws.oakes.forecast.ouya;

import tv.ouya.console.api.OuyaController;

import com.loopj.android.image.SmartImageView;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import us.feras.ecogallery.EcoGalleryAdapterView;

import ws.oakes.ouya.widget.OuyaGalleryView;

import com.squareup.otto.Produce;
import com.squareup.otto.Subscribe;

import ws.oakes.forecast.common.api.geolocation.GeolocationApi;
import ws.oakes.forecast.common.api.geolocation.GeolocationApiBuilder;
import ws.oakes.forecast.common.api.geolocation.SearchLocation;
import ws.oakes.forecast.common.api.geolocation.SearchLocations;
import ws.oakes.forecast.common.util.ForecastBus;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.widget.TextView;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.util.Log;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ProgressBar;

import java.util.List;

public class LocationActivity extends Activity implements EditText.OnEditorActionListener,
                                                          EcoGalleryAdapterView.OnItemClickListener {
    private static final String GEOCODER_USERNAME = "matto1990";
    private ForecastBus eventBus;
    private GeolocationApi geolocationApi;
    private EditText searchEditText;
    private OuyaGalleryView searchResultsList;
    private ProgressBar progressView;
    private boolean displayingResults = false;
    private ResultsAdapter searchResultsAdapter;
    public boolean firstNoGreyscaleDone = false;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.location);

        // Get and set listener for the search box
        searchEditText = (EditText) findViewById(R.id.search);
        searchEditText.setOnEditorActionListener(this);
        // Get the results list
        searchResultsList = (OuyaGalleryView) findViewById(R.id.list);
        searchResultsList.setOnItemClickListener(this);
        // Get the progress view
        progressView = (ProgressBar) findViewById(R.id.progress);

        // Get the shared event bus
        eventBus = ForecastBus.get();
        // Get the geolocation service
        geolocationApi = GeolocationApiBuilder.get();

        // Start the ouya controller system
        OuyaController.init(this);

        // Start searching
        showSearch();
    }

    @Override
    public boolean onKeyDown(final int keyCode, KeyEvent event){
        switch (keyCode){
        case OuyaController.BUTTON_A:
            if (displayingResults) {
                showSearch();
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_DONE) {
            doSearch("" + v.getText());
        }

        return false;
    }

    @Override
    public void onItemClick(EcoGalleryAdapterView<?> parent, View view, int position, long id) {
        // Get the location and send it back to the HomeActivity via the event bus
        SearchLocation location = (SearchLocation) searchResultsAdapter.getItem(position);
        eventBus.post(new LocationChangeEvent(location));

        // Close this dialog activity
        finish();
    }

    private void doSearch(String searchTerm) {
        // Show the correct view
        progressView.setVisibility(View.VISIBLE);
        searchEditText.setVisibility(View.GONE);
        searchResultsList.setVisibility(View.GONE);

        // Make the API call
        geolocationApi.search(GEOCODER_USERNAME, searchTerm, 10, "MEDIUM", new LocationSearchCallback());
    }

    private void showSearch() {
        displayingResults = false;

        // Show the correct view
        searchEditText.setVisibility(View.VISIBLE);
        progressView.setVisibility(View.GONE);
        searchResultsList.setVisibility(View.GONE);

        // Give the search field focus
        searchEditText.postDelayed(new Runnable() {
            public void run() {
                long clock = SystemClock.uptimeMillis();
                searchEditText.dispatchTouchEvent(MotionEvent.obtain(clock, clock, MotionEvent.ACTION_DOWN, 0, 0, 0));
                searchEditText.dispatchTouchEvent(MotionEvent.obtain(clock, clock, MotionEvent.ACTION_UP, 0, 0, 0));

                int length = searchEditText.getText().length();
                searchEditText.setSelection(length);
            }
        }, 200);
    }

    private void showResults(SearchLocations locations) {
        displayingResults = true;

        // Show the results
        searchResultsAdapter = new ResultsAdapter(locations.geonames);
        searchResultsList.setAdapter(searchResultsAdapter);

        // Show the correct view
        searchResultsList.setVisibility(View.VISIBLE);
        searchEditText.setVisibility(View.GONE);
        progressView.setVisibility(View.GONE);

        searchResultsList.requestFocus();
    }

    private class LocationSearchCallback implements Callback<SearchLocations> {
        @Override
        public void success(SearchLocations locations, Response responce) {
            if (locations.geonames != null && locations.geonames.size() > 0) {
                showResults(locations);
            }
            else {
                // TODO: Show an error for no results
            }
        }

        @Override
        public void failure(RetrofitError error) {
            // TODO: Handle and show this error
            Log.d("ws.oakes.forecast.api.Forecast", "Search Failed: " + error);
        }
    }

    public class ResultsAdapter extends BaseAdapter {
        private List<SearchLocation> data;
        private LayoutInflater inflater;

        public ResultsAdapter(List<SearchLocation> locations) {
            super();
            data = locations;
            android.util.Log.d("Forecast", "Size: " + data.size());
            inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Holder holder;
            if (convertView == null) {
                convertView = inflater.inflate(R.layout.item_search_result, parent, false);

                holder = new Holder();
                holder.card = convertView.findViewById(R.id.card);
                holder.text = (TextView) convertView.findViewById(R.id.text);
                holder.map = (SmartImageView) convertView.findViewById(R.id.map);

                convertView.setTag(holder);
            }
            else {
                holder = (Holder) convertView.getTag();
            }

            SearchLocation location = data.get(position);
            holder.text.setText(location.toString());

            String locString = location.lat + "," + location.lng;
            String url = "http://maps.googleapis.com/maps/api/staticmap?center=" + locString + "&markers=" + locString + "&zoom=7&size=736x414&sensor=false";
            holder.map.setImageUrl(url);

            return convertView;
        }

        public class Holder {
            View card;
            TextView text;
            SmartImageView map;
        }
    }
}
