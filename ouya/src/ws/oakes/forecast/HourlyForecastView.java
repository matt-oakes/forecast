package ws.oakes.forecast.ouya;

import ws.oakes.ouya.util.TypefaceUtils;

import ws.oakes.forecast.common.api.ForecastDataPoint;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.view.KeyEvent;
import android.view.View;
import android.util.AttributeSet;
import android.util.Log;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HourlyForecastView extends View {
	private static final float[] DEBUG_DATA = new float[] {
		0f,0f,0f,0f,0.017f,0.017f,0.02f,0.002f,0.002f,
		0.002f,0.002f,0.002f,0.002f,0.1f,0.1f,0.1f,0.1f,0.1f,0.1f,
		0.1f,0.2f,0.2f,0.2f,0.2f,0.4f,0.4f,0.1f,0.1f,0.1f,
		0.1f,0.017f,0.017f,0.017f,0.017f,0.017f,0.002f,0.002f,0.002f,0.002f,
		0.1f,0.017f,0.017f,0.017f,0.017f,0.017f,0.002f,0.002f,0.002f,0.002f,
		0.1f,0.2f,0.2f,0.2f,0.2f,0.4f,0.4f,0.1f,0.1f,0.1f,
		0.1f,0.2f,0.2f,0.2f,0.2f,0.4f,0.4f,0.1f,0.1f,0.1f
	};

	private static final int VIEW_PADDING = 50;
	private static final int BOTTOM_LABEL_PADDING = 25;
	private static final int LEVEL_LABEL_PADDING = 20;
	private static final int CURRENT_TEXT_PADDING = 10;
	private static final float Y_SCALE_UP = 2f;
	private static final SimpleDateFormat TIME_FORMAT = new SimpleDateFormat("h:mmaa");

	private List<ForecastDataPoint> data;
	private int dataLength;

	private Paint textPaint;
	private Paint leftLabelTextPaint;
	private Paint rightLabelTextPaint;
	private Paint linePaint;
	private Paint levelLinePaint;
	private Paint levelLineTextPaint;
	private Paint currentLinePaint;
	private Paint currentPointCirclePaint;
	private Paint currentLabelTextPaint;
	private Paint currentDetailTextPaint;

	private int width;
	private int height;
	private int dataWidth;
	private int dataHeight;
	private int bottomLabelHeight;
	private int stepX;
	private int dataStartX;

	private int currentDetailHeight;
	private int currentLabelHeight;

	private int lightRainY;
	private int mediumRainY;
	private int heavyRainY;
	private int lightRainXStart;
	private int mediumRainXStart;
	private int heavyRainXStart;
	private int lightRainLabelHeight;
	private int mediumRainLabelHeight;
	private int heavyRainLabelHeight;

	private int currentPosition = 0;
	private String currentTypeRain, currentTypeSnow, currentTypeSleet, currentTypeHail;
	private String noPrecipString;
	private String currentDetailStringFormat;

	public HourlyForecastView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    private void init(Context context) {
    	textPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	textPaint.setColor(0xFFFFFFFF);
    	textPaint.setStyle(Paint.Style.FILL);
		textPaint.setTextSize(36.0f);
    	try {
	    	textPaint.setTypeface(TypefaceUtils.getTypeface(context, "FuturaStd-Bold"));
	    }
	    catch (IOException e) {
	    	Log.w("Forecast", "Error getting hourly typeface. Using default.", e);
	    }

	    leftLabelTextPaint = new Paint();
	    leftLabelTextPaint.set(textPaint);

	    rightLabelTextPaint = new Paint();
	    rightLabelTextPaint.set(textPaint);
	    rightLabelTextPaint.setTextAlign(Paint.Align.RIGHT);

	    currentLabelTextPaint = new Paint();
	    currentLabelTextPaint.set(textPaint);
	    currentLabelTextPaint.setTextAlign(Paint.Align.CENTER);

	    currentDetailTextPaint = new Paint();
	    currentDetailTextPaint.setColor(0xFFFFFFFF);
    	currentDetailTextPaint.setStyle(Paint.Style.FILL);
	    currentDetailTextPaint.setTextSize(26.0f);
	    currentDetailTextPaint.setTextAlign(Paint.Align.CENTER);
	    try {
	    	Typeface typeface = TypefaceUtils.getTypeface(context, "Effra-Light");
	    	Log.d("Forecast", "Typeface: " + typeface);
	    	currentDetailTextPaint.setTypeface(typeface);
	    }
	    catch (IOException e) {
	    	Log.w("Forecast", "Error getting hourly typeface. Using default.", e);
	    }

	    levelLineTextPaint = new Paint();
	    levelLineTextPaint.set(textPaint);
	    levelLineTextPaint.setColor(0x80FFFFFF);
	    levelLineTextPaint.setTextSize(26.0f);
	    levelLineTextPaint.setTextAlign(Paint.Align.RIGHT);

    	linePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	linePaint.setColor(0xFFFFFFFF);
    	linePaint.setStyle(Paint.Style.STROKE);
		linePaint.setStrokeWidth(3.0f);

		currentPointCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	currentPointCirclePaint.setColor(0xFFFFFFFF);
    	currentPointCirclePaint.setStyle(Paint.Style.FILL);

    	currentLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	currentLinePaint.setColor(0xBFFFFFFF);
    	currentLinePaint.setStyle(Paint.Style.STROKE);
		currentLinePaint.setStrokeWidth(2.0f);

		levelLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    	levelLinePaint.setColor(0x80FFFFFF);
    	levelLinePaint.setStyle(Paint.Style.STROKE);
		levelLinePaint.setStrokeWidth(2.0f);
    	levelLinePaint.setPathEffect(new DashPathEffect(new float[] {5, 20}, 0));

    	currentTypeRain = context.getString(R.string.rain);
    	currentTypeHail = context.getString(R.string.hail);
    	currentTypeSleet = context.getString(R.string.sleet);
    	currentTypeSnow = context.getString(R.string.snow);
    	currentDetailStringFormat = context.getString(R.string.current_detail);
    	noPrecipString = context.getString(R.string.no_precip);

    	setFocusable(true);
    	setFocusableInTouchMode(true);
    }

    public void setData(List<ForecastDataPoint> requestedData) {
    	data = requestedData;

    	dataLength = data.size();
    	stepX = width / dataLength;

    	invalidate();
    	requestLayout();
    }

    // NOTE: Ignores padding it's given
    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    	width = w;
    	height = h;

    	Rect bounds = new Rect();
    	textPaint.getTextBounds("99:99pm", 0, 7, bounds);
    	bottomLabelHeight = bounds.height();

    	dataHeight = height - bottomLabelHeight - BOTTOM_LABEL_PADDING;

    	lightRainY = dataHeight - ((int) (0.017f * Y_SCALE_UP * dataHeight));
    	mediumRainY = dataHeight - ((int) (0.1f * Y_SCALE_UP * dataHeight));
    	heavyRainY = dataHeight - ((int) (0.4f * Y_SCALE_UP * dataHeight));

    	Rect lightLabelBounds = new Rect();
    	levelLineTextPaint.getTextBounds("LIGHT", 0, 5, lightLabelBounds);
    	lightRainXStart = lightLabelBounds.width() + LEVEL_LABEL_PADDING + VIEW_PADDING;
    	lightRainLabelHeight = lightLabelBounds.height();

    	Rect mediumLabelBounds = new Rect();
    	levelLineTextPaint.getTextBounds("MEDIUM", 0, 6, mediumLabelBounds);
    	mediumRainXStart = mediumLabelBounds.width() + LEVEL_LABEL_PADDING + VIEW_PADDING;
    	mediumRainLabelHeight = mediumLabelBounds.height();

    	Rect heavyLabelBounds = new Rect();
    	levelLineTextPaint.getTextBounds("HEAVY", 0, 5, heavyLabelBounds);
    	heavyRainXStart = heavyLabelBounds.width() + LEVEL_LABEL_PADDING + VIEW_PADDING;
    	heavyRainLabelHeight = heavyLabelBounds.height();

		dataStartX = mediumLabelBounds.width() + LEVEL_LABEL_PADDING + VIEW_PADDING;
    	dataWidth = width - dataStartX - (VIEW_PADDING * 2);

    	Rect currentLabelBounds = new Rect();
    	currentLabelTextPaint.getTextBounds("99:99PM", 0, 7, currentLabelBounds);
    	currentLabelHeight = currentLabelBounds.height();

    	Rect currentDetailBounds = new Rect();
    	currentDetailTextPaint.getTextBounds("99:99PM", 0, 7, currentDetailBounds);
    	currentDetailHeight = currentDetailBounds.height();

    	if (data != null) {
	    	stepX = dataWidth / dataLength;
	    }
    }

    @Override
    protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (data != null) {
			ForecastDataPoint first = data.get(0);
			ForecastDataPoint last = data.get(dataLength - 1);
			Point firstPoint = getPoint(0);
			Point finalPoint = getPoint(dataLength - 1);

			String leftLabel = getTimeString(first.time);
			String rightLabel = getTimeString(last.time);

			canvas.drawText(leftLabel, firstPoint.x, height, leftLabelTextPaint);
			canvas.drawText(rightLabel, finalPoint.x, height, rightLabelTextPaint);

			canvas.drawText("LIGHT", mediumRainXStart - LEVEL_LABEL_PADDING, lightRainY + (lightRainLabelHeight / 2), levelLineTextPaint);
			canvas.drawLine(mediumRainXStart, lightRainY, finalPoint.x, lightRainY, levelLinePaint);

			canvas.drawText("MEDIUM", mediumRainXStart - LEVEL_LABEL_PADDING, mediumRainY + (mediumRainLabelHeight / 2), levelLineTextPaint);
			canvas.drawLine(mediumRainXStart, mediumRainY, finalPoint.x, mediumRainY, levelLinePaint);

			canvas.drawText("HEAVY", mediumRainXStart - LEVEL_LABEL_PADDING, heavyRainY + (heavyRainLabelHeight / 2), levelLineTextPaint);
			canvas.drawLine(mediumRainXStart, heavyRainY, finalPoint.x, heavyRainY, levelLinePaint);

			final Path path = new Path();
			for (int i = 0; i < dataLength; i++) {
				Point thisPoint = getPoint(i);
				Point nextPoint = getPoint(i + 1);

				if (i == 0) {
					path.moveTo(thisPoint.x, thisPoint.y);
				}

				if (nextPoint != null) {
					final float midX = (nextPoint.x + thisPoint.x) / 2;
					final float midY = (nextPoint.y + thisPoint.y) / 2;
					path.quadTo(midX, midY, nextPoint.x, nextPoint.y);
				}
			}

			canvas.drawPath(path, linePaint);

			ForecastDataPoint currentMinute = data.get(currentPosition);
			Point selectedPoint = getPoint(currentPosition);
			String currentTimeString = getTimeString(currentMinute.time);
			String currentDetailString = getCurrentDetailString(currentMinute);

			canvas.drawLine(selectedPoint.x, selectedPoint.y, selectedPoint.x, currentLabelHeight + currentDetailHeight + (3 * CURRENT_TEXT_PADDING), currentLinePaint);
			canvas.drawCircle(selectedPoint.x, selectedPoint.y, 10.0f, currentPointCirclePaint);
			canvas.drawText(currentTimeString, selectedPoint.x, currentLabelHeight, currentLabelTextPaint);
			canvas.drawText(currentDetailString, selectedPoint.x, currentLabelHeight + currentDetailHeight + CURRENT_TEXT_PADDING, currentDetailTextPaint);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		boolean handled = false;

		switch (keyCode) {
		case KeyEvent.KEYCODE_DPAD_LEFT:
			currentPosition--;
			if (currentPosition < 0) {
				currentPosition = dataLength - 1;
			}
			handled = true;
			invalidate();
			break;
		case KeyEvent.KEYCODE_DPAD_RIGHT:
			currentPosition = (currentPosition + 1) % dataLength;
			handled = true;
			invalidate();
			break;
		}

		return handled || super.onKeyDown(keyCode, event);
	}

	private Point getPoint(int position) {
		if (position < 0) {
			return null;
		}
		else if (position >= dataLength) {
			return null;
		}
		else {
			int x = (stepX * position) + dataStartX;

			ForecastDataPoint minute = data.get(position);
			float value = (minute.precipIntensity > 0.4f) ? 0.4f : minute.precipIntensity;
			int yValue = (int) (value * Y_SCALE_UP * dataHeight);
			int y = height - yValue - bottomLabelHeight - BOTTOM_LABEL_PADDING;

			return new Point(x, y);
		}
	}

	private String getCurrentDetailString(ForecastDataPoint minute) {
		if (minute.precipProbability != null && minute.precipType != null) {
			String percent = "" + (int) (minute.precipProbability * 100) + "%";

			String currentType = "";
			if (minute.precipType.equals(ImageUtils.RAIN)) {
				currentType = currentTypeRain;
			}
			else if (minute.precipType.equals(ImageUtils.SLEET)) {
				currentType = currentTypeSleet;
			}
			else if (minute.precipType.equals(ImageUtils.SNOW)) {
				currentType = currentTypeSnow;
			}
			else if (minute.precipType.equals(ImageUtils.HAIL)) {
				currentType = currentTypeHail;
			}

			return String.format(currentDetailStringFormat, percent, currentType);
		}
		else {
			return noPrecipString;
		}
	}

	private String getTimeString(long timeInSeconds) {
        Date date = new Date(timeInSeconds * 1000);
        return TIME_FORMAT.format(date);
	}
}