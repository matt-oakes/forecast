package ws.oakes.forecast.ouya;

import com.squareup.otto.Subscribe;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastBus;

import com.bugsense.trace.BugSenseHandler;

import android.app.Activity;
import android.os.Bundle;
import android.view.WindowManager;

import java.util.List;

public class NextHourActivity extends Activity {
    private ForecastBus eventBus;
    private HourlyForecastView hourView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.next_hour);
        hourView = (HourlyForecastView) findViewById(R.id.hourly_view);

        BugSenseHandler.initAndStartSession(NextHourActivity.this, HomeActivity.BUGSENSE_API_KEY);

        eventBus = ForecastBus.get();
        eventBus.register(this);

        WindowManager.LayoutParams windowManager = getWindow().getAttributes();
        windowManager.dimAmount = 0.75f;
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        eventBus.unregister(this);
        BugSenseHandler.closeSession(NextHourActivity.this);
    }

    @Subscribe
    public void getForecast(ForecastEvent forecastEvent) {
        Forecast forecast = forecastEvent.getForecast();
        List<ForecastDataPoint> minutely = forecast.minutely.data;
        hourView.setData(minutely);
    }
}
