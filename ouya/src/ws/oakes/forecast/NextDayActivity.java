package ws.oakes.forecast.ouya;

import tv.ouya.console.api.OuyaController;

import com.squareup.otto.Subscribe;

import us.feras.ecogallery.EcoGalleryAdapterView;

import ws.oakes.ouya.widget.OuyaGalleryView;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastUtils;

import com.bugsense.trace.BugSenseHandler;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class NextDayActivity extends ForecastBaseActivity {
    private ForecastBus eventBus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.next_week);
        BugSenseHandler.initAndStartSession(NextDayActivity.this, HomeActivity.BUGSENSE_API_KEY);

        initForecastActivity();

        eventBus = ForecastBus.get();
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Register on the bus
        eventBus.unregister(this);
        BugSenseHandler.closeSession(NextDayActivity.this);
    }

    @Subscribe
    public void getForecast(ForecastEvent forecastEvent) {
        setupForecast(forecastEvent);
    }

    @Override
    public ForecastAdapter createAdapter(List<ForecastDataPoint> data) {
        return new HourForecastAdapter(data);
    }

    @Override
    public List<ForecastDataPoint> getForecastData(Forecast input) {
        // Create a clone of the list
        List<ForecastDataPoint> hours = new ArrayList<ForecastDataPoint>();

        // Get todays calendar at a standard time
        Calendar today = Calendar.getInstance();
        today.set(Calendar.MINUTE, 1);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        // Used for checking other times
        Calendar checkCalendar = Calendar.getInstance();
        for (ForecastDataPoint hour : input.hourly.data) {
            // Get the calendar and set to a standard time in the day
            checkCalendar.setTimeInMillis(hour.time * 1000);
            checkCalendar.set(Calendar.MINUTE, 1);
            checkCalendar.set(Calendar.SECOND, 0);
            checkCalendar.set(Calendar.MILLISECOND, 0);

            // If before today, remove it
            if (!checkCalendar.before(today)) {
                hours.add(hour);
            }
        }

        return hours;
    }

    private class HourForecastAdapter extends ForecastAdapter {
        public HourForecastAdapter(List<ForecastDataPoint> data) {
            super(data);
        }

        @Override
        public int getLayoutResource() {
            return R.layout.item_weather_hour;
        }

        @Override
        public void setDateLabel(Holder holder, ForecastDataPoint data) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.time * 1000);
            Date date = calendar.getTime();

            SimpleDateFormat dateFormat = new SimpleDateFormat("haa");
            String dateString = dateFormat.format(date);

            if (dateString.equalsIgnoreCase("12am")) {
                dateString = getString(R.string.midnight);
            }
            else if (dateString.equalsIgnoreCase("12pm")) {
                dateString = getString(R.string.noon);
            }
            else if (isNow(calendar)) {
                dateString = getString(R.string.now);
            }

            holder.date.setText(dateString);
        }

        private boolean isNow(Calendar other) {
            // Get todays calendar at a standard time
            Calendar now = Calendar.getInstance();
            now.set(Calendar.MINUTE, 0);
            now.set(Calendar.SECOND, 0);
            now.set(Calendar.MILLISECOND, 0);

            Calendar otherClone = (Calendar) other.clone();
            otherClone.set(Calendar.MINUTE, 0);
            otherClone.set(Calendar.SECOND, 0);
            otherClone.set(Calendar.MILLISECOND, 0);

            return otherClone.equals(now);
        }
    }
}
