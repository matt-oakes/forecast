package ws.oakes.forecast.ouya;

import tv.ouya.console.api.OuyaController;

import com.squareup.otto.Subscribe;

import us.feras.ecogallery.EcoGalleryAdapterView;

import ws.oakes.ouya.widget.OuyaGalleryView;

import ws.oakes.forecast.common.api.Forecast;
import ws.oakes.forecast.common.api.ForecastDataPoint;
import ws.oakes.forecast.common.util.ForecastBus;
import ws.oakes.forecast.common.util.ForecastUtils;

import com.bugsense.trace.BugSenseHandler;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.BaseAdapter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class NextWeekActivity extends ForecastBaseActivity {
    private ForecastBus eventBus;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.next_week);
        BugSenseHandler.initAndStartSession(NextWeekActivity.this, HomeActivity.BUGSENSE_API_KEY);

        initForecastActivity();

        eventBus = ForecastBus.get();
        eventBus.register(this);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Register on the bus
        eventBus.unregister(this);
        BugSenseHandler.closeSession(NextWeekActivity.this);
    }

    @Subscribe
    public void getForecast(ForecastEvent forecastEvent) {
    	setupForecast(forecastEvent);
    }

    @Override
    public ForecastAdapter createAdapter(List<ForecastDataPoint> data) {
    	return new DayForecastAdapter(data);
    }

    @Override
    public List<ForecastDataPoint> getForecastData(Forecast input) {
    	// Create a clone of the list
    	List<ForecastDataPoint> days = new ArrayList<ForecastDataPoint>();

    	// Get todays calendar at a standard time
    	Calendar today = Calendar.getInstance();
    	today.set(Calendar.HOUR_OF_DAY, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

    	// Used for checking other times
    	Calendar checkCalendar = Calendar.getInstance();
    	for (ForecastDataPoint day : input.daily.data) {
    		// Get the calendar and set to a standard time in the day
            checkCalendar.setTimeInMillis(day.time * 1000);
            checkCalendar.set(Calendar.HOUR_OF_DAY, 0);
            checkCalendar.set(Calendar.MINUTE, 0);
            checkCalendar.set(Calendar.SECOND, 0);
            checkCalendar.set(Calendar.MILLISECOND, 0);

            // If before today, remove it
            if (!checkCalendar.before(today)) {
            	days.add(day);
            }
    	}

    	return days;
    }

    private class DayForecastAdapter extends ForecastAdapter {
    	private Calendar today;

    	public DayForecastAdapter(List<ForecastDataPoint> data) {
    		super(data);

    		today = Calendar.getInstance();
    	}

    	@Override
    	public int getLayoutResource() {
    		return R.layout.item_weather_day;
    	}

    	@Override
	    public void setDateLabel(Holder holder, ForecastDataPoint data) {
            // Get the calendar for the day we're displaying
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(data.time * 1000);
            // Get the date string we need
            String dateString;
            if (isToday(calendar)) {
                dateString = getString(R.string.today);
            }
            else if (isTomorrow(calendar)) {
                dateString = getString(R.string.tomorrow);
            }
            else {
                Date date = calendar.getTime();
                SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEEEEEEEEEE");
                dateString = dateFormat.format(date);
            }
            holder.date.setText(dateString);
        }

        private boolean isToday(Calendar checkDate) {
        	int day = today.get(Calendar.DATE);
        	int month = today.get(Calendar.MONTH);
        	int year = today.get(Calendar.YEAR);

        	int checkDay = checkDate.get(Calendar.DATE);
        	int checkMonth = checkDate.get(Calendar.MONTH);
        	int checkYear = checkDate.get(Calendar.YEAR);

        	return (day == checkDay) && (month == checkMonth) && (year == checkYear);
        }

        private boolean isTomorrow(Calendar checkDate) {
        	Calendar tomorrow = (Calendar) today.clone();
        	tomorrow.add(Calendar.DATE, 1);

        	int day = tomorrow.get(Calendar.DATE);
        	int month = tomorrow.get(Calendar.MONTH);
        	int year = tomorrow.get(Calendar.YEAR);

        	int checkDay = checkDate.get(Calendar.DATE);
        	int checkMonth = checkDate.get(Calendar.MONTH);
        	int checkYear = checkDate.get(Calendar.YEAR);

        	return (day == checkDay) && (month == checkMonth) && (year == checkYear);
        }
    }
}
