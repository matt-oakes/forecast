package ws.oakes.forecast.ouya;

import ws.oakes.forecast.common.api.geolocation.SearchLocation;

public class LocationChangeEvent {
	private SearchLocation location;

    public LocationChangeEvent(SearchLocation location) {
        this.location = location;
    }

    public SearchLocation getLocation() {
        return location;
    }
}